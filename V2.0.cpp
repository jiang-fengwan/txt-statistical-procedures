/*******************************************************************************
程序名称：命令行文本计数统计程序

功能描述：对导入的纯英文txt文本进行统计，输出其字符数，单词数，句子数。

开发人员：Fengwan-Jiang 

开发版本；V2.0 
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
int main()
{
	FILE *fp;   //文件指针 
	char str1,str2;
	int chars=0,word=0,sen=0;  //字符数、单词数、句子数 
//UI界面 
	printf("***************************\n");
	printf("****统计字符数，请输入c****\n");
	printf("****统计单词数，请输入w****\n");
	printf("****统计句子数，请输入s****\n");
	printf("***************************\n");

	scanf("%c",&str1);
	if((fp=fopen("d:\\xc.txt","r"))==NULL)  //打开文件，txt文本地址为d:\\xc.txt，以只读的形式打开 
	{
		printf("File open error!\n"); //打开文件失败 
		exit(0);//系统标准函数，关闭所有打开的文件 
	}
	str2=fgetc(fp);//从导入文件中的提取一个字符到str2 
	while(str2!=EOF)//循环
	{

		if(str2==' '||str2==','||str2=='.'||str2=='!'||str2=='?')//通过统计空格数和逗号统计单词数
		{
			word++;
		}
		if(str2=='.'||str2=='!'||str2=='?')//通过结尾的标点符号统计句子数
		{
			sen++;
		}
		if(str2!=' ')//除空格外的所有字符 
		chars++;
		str2=fgetc(fp);//继续提取字符进入循环
	}
	if(str1=='c')
		printf("字符数：%d个\n",chars-1);
	else if(str1=='w')
		printf("单词数：%d个\n",word);
	else if(str1=='s')
		printf("句子数：%d句\n",sen);

	fclose(fp);//关闭文件 
	return 0;
}

