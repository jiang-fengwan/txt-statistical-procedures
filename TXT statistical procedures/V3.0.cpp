/*******************************************************************************
程序名称：命令行文本计数统计程序

功能描述：对导入的纯英文txt文本进行统计，输出其字符数，单词数，句子数。文件的路
          径可选择。对代码文件可统计总行、注释行，并提供相应命令接口。 

开发人员：Fengwan-Jiang 

开发版本；V3.0 
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
int main()
{
	FILE *fp;   //文件指针 
	char str1,str2,filename[100]; //命令符、文件字符、文件名 
	int chars=0,word=0,sen=0,line=0,zline=0;  //字符数、单词数、句子数、总行数、注释行行数 
	printf("********************************\n");
    printf("请输入你要打开的文件名及路径:\n如c:\\User\\Fengwan-Jiang\\temp.txt\n");
   	printf("********************************\n");
    gets(filename);  //输入文件名及路径 
   	system("cls");//清屏函数 
    if ((fp=fopen(filename,"r"))==NULL)//以只读的方式打开文件，并判断是否有打开错误
    {
	    printf("打开文件%s出现错误\n",filename);   
        exit(0);//系统标准函数，关闭所有打开的文件 
    }  
//UI界面 
	printf("***************************\n");
	printf("****统计字符数，请输入c****\n");
	printf("****统计单词数，请输入w****\n");
	printf("****统计句子数，请输入s****\n");
	printf("****查询总行数，请输入l****\n");
	printf("**查询注释行行数，请输入z**\n");
	printf("***************************\n");
	scanf("%c",&str1);
	str2=fgetc(fp);//从导入文件中的提取一个字符到str2 
	while(str2!=EOF)//循环
	{
		if(str2=='\n')//通过计数换行符统计行数
		{
			line++;
		}
		if(str2=='*')
		{
			zline++;
		}
		if(str2==' '||str2==','||str2=='.'||str2=='!'||str2=='?')//通过统计空格数和逗号统计单词数
		{
			word++;
		}
		if(str2=='.'||str2=='!'||str2=='?')//通过结尾的标点符号统计句子数
		{
			sen++;
		}
		if(str2!=' ')//除空格外的所有字符 
		chars++;
		str2=fgetc(fp);//继续提取字符进入循环
	}
	if(str1=='c')
		printf("字符数：%d个\n",chars-1);
	else if(str1=='w')
		printf("单词数：%d个\n",word);
	else if(str1=='s')
		printf("句子数：%d句\n",sen);
	else if(str1=='l')
		printf("总行数：%d行\n",line);
	else if(str1=='z')
		printf("注释行数：%d行\n",zline/2);
	fclose(fp);//关闭文件 
	return 0;
}

